# rt-preprocess

Pre-process email before passing it to Request Tracker, to remove text after
"Please type your reply above this line" markers.

# DESCRIPTION

This program reads an email on standard input, alters it according to the
command-line options, and writes the altered version to standard output.

It is intended for use in a Request Tracker mail gateway, to be inserted in
a pipeline before the `rt-mailgate` command.

Usually it would be used with "_Please type your reply above this line_"
markers - this program can strip such markers, and all text that follows
them, from incoming emails, decluttering ticket correspondence.

When stripping text from HTML email, this program automatically removes any
associated attachments (such as images) which were referenced by the
stripped part of the message and which were not referenced in any remaining
part of the message.

# OPTIONS

- -s, --strip STRING

    strip all text from STRING onwards

- -h, --help

    output some help

- -V, --version

    output version information

Note that the `--strip` option can be given multiple times, to match and
remove more than one marker.

# EXAMPLE INSTALLATION

If your `/etc/aliases` file contains an entry like this:

    support: |"/opt/rt5/bin/rt-mailgate --queue \"General\" --action correspond --url https://some.site/rt5"

then this program could be inserted like this:

    support: |"/usr/bin/rt-preprocess -s \"##- Please type your reply above this line -##\" | /opt/rt5/bin/rt-mailgate --queue \"General\" --action correspond --url https://some.site/rt5"

This assumes that you have also adjusted your RT templates to start with
this text on a line by itself:

    ##- Please type your reply above this line -##

The `/usr/bin/rt-preprocess` part should be adjusted to point to wherever
you have installed this program.

Remember to set the program's executable bits (`chmod 755`).

# ALTERNATIVES

See https://ivarch.com/programs/RT-Extension-EmailReplyDelimiter.shtml
for an alternative approach using an RT extension.

# ISSUES AND CONTRIBUTIONS

The project is held on [Codeberg](https://codeberg.org); its issue tracker
is at [https://codeberg.org/ivarch/rt-preprocess/issues](https://codeberg.org/ivarch/rt-preprocess/issues).

# LICENSE AND COPYRIGHT

Copyright 2023 Andrew Wood.

License GPLv3+: GNU GPL version 3 or later: [https://gnu.org/licenses/gpl.html](https://gnu.org/licenses/gpl.html)

This is free software: you are free to change and redistribute it.  There is
NO WARRANTY, to the extent permitted by law.
