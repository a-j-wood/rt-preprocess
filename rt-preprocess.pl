#!/usr/bin/perl
#
# Read an email on standard input, alter it according to provided options,
# and output the email on standard output.
#
# Inspired by:
# https://forum.bestpractical.com/t/is-there-any-way-to-have-rt-use-some-kind-of-reply-above-this-line-email-delimiter/37784
#

our $ProgramName    = 'rt-preprocess';
our $ProgramVersion = '0.01';

use File::Temp;
use File::Copy;
use File::Path;
use MIME::Parser;

=head1 NAME

rt-preprocess - Pre-process email before passing it to Request Tracker

=head1 DESCRIPTION

This program reads an email on standard input, alters it according to the
command-line options, and writes the altered version to standard output.

It is intended for use in a Request Tracker mail gateway, to be inserted in
a pipeline before the C<rt-mailgate> command.

Usually it would be used with "I<Please type your reply above this line>"
markers - this program can strip such markers, and all text that follows
them, from incoming emails, decluttering ticket correspondence.

When stripping text from HTML email, this program automatically removes any
associated attachments (such as images) which were referenced by the
stripped part of the message and which were not referenced in any remaining
part of the message.

=head1 OPTIONS

=over 20

=item -s, --strip STRING

strip all text from STRING onwards

=item -h, --help

output some help

=item -V, --version

output version information

=back

Note that the C<--strip> option can be given multiple times, to match and
remove more than one marker.

=head1 EXAMPLE INSTALLATION

If your F</etc/aliases> file contains an entry like this:

    support: |"/opt/rt5/bin/rt-mailgate --queue \"General\" --action correspond --url https://some.site/rt5"

then this program could be inserted like this:

    support: |"/usr/bin/rt-preprocess -s \"##- Please type your reply above this line -##\" | /opt/rt5/bin/rt-mailgate --queue \"General\" --action correspond --url https://some.site/rt5"

This assumes that you have also adjusted your RT templates to start with
this text on a line by itself:

    ##- Please type your reply above this line -##

The F</usr/bin/rt-preprocess> part should be adjusted to point to wherever
you have installed this program.

Remember to set the program's executable bits (C<chmod 755>).

=head1 ISSUES AND CONTRIBUTIONS

The project is held on L<Codeberg|https://codeberg.org>; its issue tracker
is at L<https://codeberg.org/ivarch/rt-preprocess/issues>.

=head1 LICENSE AND COPYRIGHT

Copyright 2023 Andrew Wood.

License GPLv3+: GNU GPL version 3 or later: L<https://gnu.org/licenses/gpl.html>

This is free software: you are free to change and redistribute it.  There is
NO WARRANTY, to the extent permitted by law.

=cut

our ( $OriginalEmailHandle, $OriginalEmailFile ) = ( undef, undef );
our $ParserWorkDir = undef;

our $StripMarkers = [];

while ( my $Option = shift @ARGV ) {
    if ( $Option =~ /^(-s|--strip)$/ ) {
        my $Value = shift @ARGV;
        die "$ProgramName: --strip: value required" if ( not defined $Value );
        push @$StripMarkers, $Value;
    }
    elsif ( $Option =~ /^(-h|--help)$/ ) {
        print "Usage: $ProgramName [OPTION...]\n";
        print "Filter an email from standard input to standard output.\n";
        print "\n";
        print "  -s, --strip STRING  strip all text from STRING onwards\n";
        print "\n";
        print "  -h, --help          output this help\n";
        print "  -V, --version       output version information\n";
        print "\n";
        print "For example:\n";
        print "$ProgramName -s "
          . "'##- Please type your reply above this line -##'"
          . " < in > out\n";
        print "\n";
        print "Report bugs to: https://codeberg.org/ivarch/rt-preprocess/issues\n";
        exit 0;
    }
    elsif ( $Option =~ /^(-V|--version)$/ ) {
        print "$ProgramName $ProgramVersion\n";
        print "Copyright 2023 Andrew Wood\n";
        print
"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.\n";
        print
"This is free software: you are free to change and redistribute it.\n";
        print "There is NO WARRANTY, to the extent permitted by law.\n";
        exit 0;
    }
    elsif ( $Option =~ /^-/ ) {
        die "$ProgramName: $Option: unknown option\n";
    }
    else {
        die "$ProgramName: $Option: unexpected argument\n";
    }
}

#
# Read the email on standard input and store it in a temporary file, so that
# if we fail to parse it, we can just output it as it was.
#

( $OriginalEmailHandle, $OriginalEmailFile ) = File::Temp::tempfile();
File::Copy::copy( \*STDIN, $OriginalEmailHandle ) || die;
close($OriginalEmailHandle);
$OriginalEmailHandle = undef;

#
# Parse the copy of the email.
#

$ParserWorkDir = File::Temp::tempdir( CLEANUP => 0 );

my $Parser = new MIME::Parser;

# Favour using temporary files rather than memory.
$Parser->output_dir($ParserWorkDir);
$Parser->output_to_core(0);
$Parser->tmp_to_core(0);

my $TopEntity = undef;
eval { $TopEntity = $Parser->parse_open($OriginalEmailFile) };

# If the parsing failed or there was no subject line detected, just output
# the email on stdout with no processing.
if (   $@
    || ( not defined $TopEntity )
    || ( not defined $TopEntity->head )
    || ( not $TopEntity->head->count('Subject') ) )
{
    File::Copy::copy( $OriginalEmailFile, \*STDOUT );
    exit;
}

#
# Process the parsed message.
#

if ( $TopEntity->bodyhandle ) {

    # If there's a top-level body, this is a simple message with no
    # attachments, so just process the body directly.

    ProcessEntity( $TopEntity, {}, {} );

}
else {

    # If there isn't a top-level body, this is a multipart message, so process
    # each of the parts which has a body, and then remove any of those which
    # were flagged for removal by processing.

    my @MessageParts = $TopEntity->parts;

    # Content IDs seen in kept sections of entity bodies.
    my %ContentIdInKeptSection = ();

    # Content IDs seen in removed sections of entity bodies.
    my %ContentIdInRemovedSection = ();

    foreach (@MessageParts) {
        ProcessEntity( $_, \%ContentIdInKeptSection,
            \%ContentIdInRemovedSection );
    }

    # Content IDs seen ONLY in removed sections of entity bodies.
    my %ContentIdToRemove = ();
    foreach ( keys %ContentIdInRemovedSection ) {
        $ContentIdToRemove{$_} = 1
          if ( not exists $ContentIdInKeptSection{$_} );
    }

    # Remove parts the need to be removed.
    if ( scalar keys %ContentIdToRemove > 0 ) {
        my @RemainingParts =
          grep { ShouldKeepPart( $_, \%ContentIdToRemove ) } @MessageParts;
        $TopEntity->parts( \@RemainingParts );
    }

}

#
# Output the altered message.
#

$TopEntity->print( \*STDOUT );

# On-exit cleanup.
#
END {
    close($OriginalEmailHandle)        if ( defined $OriginalEmailHandle );
    unlink $OriginalEmailFile          if ( defined $OriginalEmailFile );
    File::Path::rmtree($ParserWorkDir) if ( defined $ParserWorkDir );
    $OriginalEmailHandle = undef;
    $OriginalEmailFile   = undef;
    $ParserWorkDir       = undef;
}

# Process a MIME::Entity, adjusting its body if necessary, and populating
# %$ContentIdInKeptSection and %$ContentIdInRemovedSection with the
# content-IDs detected in the body section that was kept, and in the body
# section that was removed (if any), respectively.
#
sub ProcessEntity {
    my ( $Entity, $ContentIdInKeptSection, $ContentIdInRemovedSection, $Depth )
      = @_;
    my (
        $EffectiveType,  $TextType, $BodyContent,
        $RemovedContent, $FailedUpdate
    );

    $EffectiveType = $Entity->effective_type;

    $Depth = 0 if ( not defined $Depth );
    $Depth++;
    return if ( $Depth > 3 );

    # Recurse into multipart entities.
    if ( $EffectiveType =~ /^multipart/ ) {
        ProcessEntity( $_, $ContentIdInKeptSection, $ContentIdInRemovedSection,
            $Depth )
          foreach ( $Entity->parts );
        return;
    }

    return if ( $EffectiveType !~ /^text\/(plain|html)/ );
    $TextType = $1;

    $BodyContent = $Entity->bodyhandle->as_string;
    return if ( not defined $BodyContent );

    $RemovedContent = undef;

    # If any entity updates fail, we will write the original message to stdout
    # and exit early.
    $FailedUpdate = 0;

    # If a strip marker is found, remove it and anything following it.
    foreach my $StripMarker ( @{ $StripMarkers || [] } ) {

        if ( $TextType eq 'plain' ) {
            $RemovedContent = $1
              if ( $BodyContent =~ s/(\s+\Q$StripMarker\E.+)$//s );
        }
        else {
            $RemovedContent = $2
              if ( $BodyContent =~ s/(\s+|>\s*)(\Q$StripMarker\E.+)$/$1/s );

            if ( defined $RemovedContent ) {
                $BodyContent .= '</body></html>'
                  if ( $RemovedContent =~ /<\/body/i );
            }
        }

        if ( defined $RemovedContent ) {
            my $IO;
            $IO = $Entity->bodyhandle->open('w');
            if ($IO) {
                $IO->print($BodyContent);
                if ( not $IO->close ) {
                    $FailedUpdate = 1;
                }
            }
            else {
                $FailedUpdate = 1;
            }

            last;
        }
    }

    # If we tried and failed to update the body, write out the original
    # message and exit early.
    if ($FailedUpdate) {
        File::Copy::copy( $OriginalEmailFile, \*STDOUT );
        exit;
    }

    # If this is an HTML part, look for content IDs referenced in tags, and
    # update the hash references as described above.
    if ( $TextType eq 'html' ) {
        foreach ( split /</, $BodyContent ) {
            next if ( !/\s(?:src|href)=(?:['"])?cid:([^'">\s]+)/i );
            $ContentIdInKeptSection->{$1} = 1;
        }
        if ( defined $RemovedContent ) {
            foreach ( split /</, $RemovedContent ) {
                next if ( !/\s(?:src|href)=(?:['"])?cid:([^'">\s]+)/i );
                $ContentIdInRemovedSection->{$1} = 1;
            }
        }
    }
}

# Return true if the given MIME::Entity should be kept in the message: it
# should be kept if either it has no "Content-ID", or if its "Content-ID"
# value does NOT appear as a key in the hashref %$ContentIdToRemove.
#
sub ShouldKeepPart {
    my ( $Entity, $ContentIdToRemove ) = @_;
    my ( $ContentIdHeader, $ContentId );

    $ContentIdHeader = $Entity->head->get( 'content-id', 0 );
    return 1 if ( not defined $ContentIdHeader );
    return 1 if ( $ContentIdHeader !~ /^(<)?(.+?)(?(1)>)\s*$/ );

    $ContentId = $2;

    return 1 if ( not exists $ContentIdToRemove->{$ContentId} );

    return 0;
}

# EOF
